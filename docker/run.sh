#!/usr/bin/env bash

APP_ENV="${APP_ENV:-production}"
SUBJECTIVITY_PORT=${SUBJECTIVITY_PORT:-5051}
SUBJECTIVITY_WEB_CONCURRENCY=${SUBJECTIVITY_WEB_CONCURRENCY:-1}
export LRU_CACHE_CAPACITY=1
if [ "${APP_ENV}" = "development" ];then
  LD_PRELOAD=/usr/local/lib/libjemalloc.so uvicorn app.main:app --workers "$SUBJECTIVITY_WEB_CONCURRENCY" --host 0.0.0.0 --port "$SUBJECTIVITY_PORT" --reload
else
  # LD_PRELOAD=/usr/local/lib/libjemalloc.so uvicorn app.main:app --workers "$SUBJECTIVITY_WEB_CONCURRENCY" --host 0.0.0.0 --port "$SUBJECTIVITY_PORT"
  # LD_PRELOAD=/usr/local/lib/libjemalloc.so gunicorn -c /app/corn.py app.main:app
  LD_PRELOAD=/usr/local/lib/libjemalloc.so hypercorn --config file:/app/corn.py app.main:app
fi
