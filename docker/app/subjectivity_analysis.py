"""-*- coding: utf-8 -*-."""
import os
from typing import Tuple
import multiprocessing
import re
import numpy as np
import torch
import torch.nn.functional as f
from pytorch_pretrained_bert import (  # noqa # type: ignore
    BertForSequenceClassification,
    BertConfig,
    BertTokenizer,
)

output_dir = os.path.realpath(
    os.path.join(os.path.dirname(__file__), "..", "models", "SUBJ")
)
tokenizer_dir = os.path.realpath(
    os.path.join(os.path.dirname(__file__), "..", "models", "bert-base-uncased")
)
output_model_file = os.path.join(output_dir, "pytorch_model.bin")
output_config_file = os.path.join(output_dir, "config.json")
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
config = BertConfig(output_config_file)
tokenizer = BertTokenizer.from_pretrained(tokenizer_dir)
model = BertForSequenceClassification(config, num_labels=2)
model.load_state_dict(torch.load(output_model_file, map_location=device))
model.eval()


def separate_process(func):
    """Multiprocessing annotator."""

    def parallel_wrapper(output_dict, *argv, **kwargs):
        ret = func(*argv, **kwargs)
        if ret is not None:
            output_dict["ret"] = ret

    def outer_wrapper(*argv, **kwargs):
        same_process = kwargs.pop("same_process", False)
        if same_process:
            return func(*argv, **kwargs)

        with multiprocessing.Manager() as manager:
            output = manager.dict()
            args = (output,) + argv
            p = multiprocessing.Process(
                target=parallel_wrapper, args=args, kwargs=kwargs
            )
            p.start()
            p.join()
            ret_val = output.get("ret", None)

        return ret_val

    return outer_wrapper


def preprocess_text(text: str, sep="[SEP]", cls="[CLS]") -> str:
    """Text preprocessing."""
    text = " ".join((cls, text, sep))

    text = re.sub(r"([.!?])", r" \1", text)
    text = re.sub(r"[^a-zA-Z.!?[]]+", r" ", text)
    text = re.sub(r"\s+", r" ", text).strip()
    return text


def tokenize_text(text: str) -> Tuple[torch.Tensor, torch.Tensor]:
    """Tokenize text."""
    tokenized_text = tokenizer.tokenize(text)
    indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_text)
    # Define sentence A and B indices associated to 1st and 2nd sentences (see paper)
    # segments_ids = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1]
    sep = np.argmax(np.asarray(tokenized_text) == "[SEP]")
    segments_ids = [1 if token > sep else 0 for token in range(len(tokenized_text))]

    # Convert inputs to PyTorch tensors
    segments_tensors = torch.tensor([segments_ids])
    tokens_tensor = torch.tensor([indexed_tokens])
    return tokens_tensor, segments_tensors


def tokenize(text: str) -> Tuple[torch.Tensor, torch.Tensor]:
    """Preprocess and tokenize input text."""
    preprocessed = preprocess_text(text=text)
    tokenized = tokenize_text(text=preprocessed)
    return tokenized


def compute_subjectivity(text: str) -> float:
    """Compute the subjectivity of the input text."""
    tokens_tensor, segments_tensors = tokenize(text=text)
    output = model(tokens_tensor, segments_tensors)
    prediction = f.softmax(output, dim=1)
    result = prediction.data[0].numpy()[1]
    return float(result)
