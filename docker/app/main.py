"""-*- coding: utf-8 -*-."""
from fastapi import FastAPI
from fastapi.responses import ORJSONResponse, Response
from pydantic import BaseModel
import orjson
from .subjectivity_analysis import compute_subjectivity  # noqa # type: ignore

app = FastAPI(redoc_url=None, docs_url=None, default_response_class=ORJSONResponse)


@app.head("/health", include_in_schema=False, status_code=204)
async def head_check():
    """Health check."""
    return None


class RequestBody(BaseModel):
    """The Expected request body."""

    text: str


@app.post("/subjectivity")
async def eunomia_subjectivity(post: RequestBody):
    """Subjectivity analysis post handler.

    output: the analysis result, float [0, 1]
    a result of 0.51 means the post text is subjective with confidence 51%
    a result of 0.48 means the post text is objective with confidence 52%
    """
    result = compute_subjectivity(text=post.text)
    data = orjson.dumps({"result": result}, option=orjson.OPT_SERIALIZE_NUMPY)
    return Response(content=data, media_type="application/json")
