#!/bin/sh
# shellcheck disable=SC2001
text1="EUNOMIA is a H2020 IA project."
text2="I was thrilled to hear about EUNOMIA project."
text3="Earth is flat"
text4="I think that earth is not flat"

data1="{\"text\": \"${text1}\"}"
data2="{\"text\": \"${text2}\"}"
data3="{\"text\": \"${text3}\"}"
data4="{\"text\": \"${text4}\"}"
data5="{\"invalid\": \"input text\"}"

# docker-compose up -d
# sleep 20
echo "Input,Output" > test_results.csv

response1=$(curl -sS --request POST --url http://localhost:5051/subjectivity --header 'Content-Type: application/json' --data "${data1}")
echo "${data1},${response1}" >> test_results.csv

response2=$(curl -sS --request POST --url http://localhost:5051/subjectivity --header 'Content-Type: application/json' --data "${data2}")
echo "${data2},${response2}" >> test_results.csv

response3=$(curl -sS --request POST --url http://localhost:5051/subjectivity --header 'Content-Type: application/json' --data "${data3}")
echo "${data3},${response3}" >> test_results.csv

response4=$(curl -sS --request POST --url http://localhost:5051/subjectivity --header 'Content-Type: application/json' --data "${data4}")
echo "${data4},${response4}" >> test_results.csv

# expected validation error
response5=$(curl -sS --request POST --url http://localhost:5051/subjectivity --header 'Content-Type: application/json' --data "${data5}" | sed 's/,/;/g')
echo "${data5},${response5}" >> test_results.csv
