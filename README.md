# EUNOMIA Subjectivity Analysis

## Environment variables

- SUBJECTIVITY_PORT=5051
- SUBJECTIVITY_WEB_CONCURRENCY=1 # test performance and change it if needed
- SUBJECTIVITY_ANALYSIS_IMAGE= # where to upload the built image
- SUBJECTIVITY_ANALYSIS_TAG=latest

## Usage

To get the subjectivity analysis results for a specific post, an http POST request has to be made:

- url: <http://subjectivity-server:5051/subjectivity>
- json body: {"text": "the text of the post"}
The result of the analysis is json format:
- {"result": <subjectivity_result>} </br>
where:
  - <subjectivity_result> is a float between 0 and 1 (0.7 means subjective with 70% confidence, 0.2 means objective with 80% confidence)

## Other

- Simple test calls and results in [./tests](./tests) </br>
- Docker compose logs, docker-stats and curl responses in [./stats](./stats)
